<?php
   ini_set("session.cookie_httponly", 1);
   session_start();
   require 'database.php';
   $token = $_SESSION['token'];
   if ($token != $_SESSION['token']){
      echo json_encode(array('success' => 'false', 'error' => 'csrf validation failed'));
      return;
   }
   $user_id = $_SESSION['user_id'];
   $title = htmlentities($_POST['title']);
   $date = htmlentities($_POST['date']);
   $time = htmlentities($_POST['time']);
   $cat = htmlentities($_POST['category']);
   if(isset($_POST['title'])){
      if(isset($_POST['time'])){
         if(isset($_POST['date'])){
            if(isset($_POST['category'])){
               $stmt = $mysqli->prepare("insert into events(user_id, date, time, title, category) values(?, ?, ?, ?,?)");
               if(!$stmt){
                  printf("Query Prep Failed: %s\n", $mysqli->error);
                  exit;
               }
               $stmt->bind_param('isssi',$user_id,$date,$time,$title,$cat);
               $stmt->execute();
               $stmt->close();
               //$data = array('success' => 'true');
               //echo json_encode($data);
              echo json_encode(array('success' => 'true', 'category'=>$cat));
            }
            else{
               echo json_encode(array('success' => 'false', 'error' => 'category empty'));
            }
         }
         else{
            echo json_encode(array('success' => 'false', 'error' => 'date empty'));
          //  return; 
         }
      }
      else{
         echo json_encode(array('success' => 'false', 'error' => 'time empty'));
        // return; 
      }
    }
    else{
      echo json_encode(array('success' => 'false', 'error' => 'title empty'));
     // return; 
    } 
?>