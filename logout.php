<!--This form presents the logout page, where the user logs out and destroys their session.-->
<?php
    session_destroy();
    header("Location: login.php");
?>