<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Calendar</title>
<!--<link rel="stylesheet" type="text/css" href="weather.css" />-->
<style>
body{ 
   background-color: #F6FAFF;
}
#dayheader{
   color: #000000;
	background-color: #E0F0FF;
}
/*#eventsBox{
   display: none;
   border:1px solid black;
   width: auto;
}*/
#eventsSpace{
   font: 14px;
}
#buttons{
   width:100%;
   text-align: center;
}
.inner{
   display:inline-block;
}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script type="text/javascript">
   (function(){Date.prototype.deltaDays=function(c){return new Date(this.getFullYear(),this.getMonth(),this.getDate()+c)};Date.prototype.getSunday=function(){return this.deltaDays(-1*this.getDay())}})();
   function Week(c){this.sunday=c.getSunday();this.nextWeek=function(){return new Week(this.sunday.deltaDays(7))};this.prevWeek=function(){return new Week(this.sunday.deltaDays(-7))};this.contains=function(b){return this.sunday.valueOf()===b.getSunday().valueOf()};this.getDates=function(){for(var b=[],a=0;7>a;a++)b.push(this.sunday.deltaDays(a));return b}}
   function Month(c,b){this.year=c;this.month=b;this.nextMonth=function(){return new Month(c+Math.floor((b+1)/12),(b+1)%12)};this.prevMonth=function(){return new Month(c+Math.floor((b-1)/12),(b+11)%12)};this.getDateObject=function(a){return new Date(this.year,this.month,a)};this.getWeeks=function(){var a=this.getDateObject(1),b=this.nextMonth().getDateObject(0),c=[],a=new Week(a);for(c.push(a);!a.contains(b);)a=a.nextWeek(),c.push(a);return c}};
   var today = new Date();
   var mm = today.getMonth();
   var yyyy = today.getFullYear();
   var currentMonth = new Month(yyyy,mm);
   var monthNames = ["January", "February", "March", "April", "May", "June",
                     "July", "August", "September", "October", "November", "December"];
   var categories=["Home","School","Work","Relaxation","Excercise"];
   var dayEvents;
   var updateCalendar = function(){
      document.getElementById("grid").innerHTML = '<tr id ="dayheader" ><th>Sunday</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th></tr>';
      document.getElementById("monthtitle").textContent = monthNames[currentMonth.month]+ " " +currentMonth.year;
      var weeks = currentMonth.getWeeks();
      for(var w=0;w<weeks.length;w++){
         var week = document.createElement("tr");
         var days = weeks[w].getDates();
         for(var d=0;d<days.length;d++){
            var day = document.createElement("td");
            day.setAttribute("height","80");
            var thisdate = days[d].getDate();
            day.appendChild(document.createTextNode(thisdate));
            day.appendChild(document.createElement("br"));
            
            var getEventsButton = document.createElement("button");
            getEventsButton.innerHTML= "Show Events";
            
            //displays the event
            getEventsButton.onclick = function(){
                  var ajaxDate = this;
                  var ajaxMonth = currentMonth.month+1;
                  var ajaxYear = currentMonth.year;
                  console.log(ajaxDate);
                  console.log(ajaxMonth);
                  $.ajax({
                     type:"GET",
                     url: "getEventforDay.php",
                     data:{
                        day: ajaxDate,
                        month: ajaxMonth,
                        year: ajaxYear
                     }, dataType: 'json',
                     success: function(data){
                        console.log(data.events);
                        if (data.success == 'false'){
                           if(data.error == 'csrf validation failed') {
                              window.alert(data.error);
                              return;
                           }
                           window.alert(data.error);
                           return;
                        }
                        else{
                           dayEvents = data.events;
                           var thisevent =monthNames[ajaxMonth-1] + " "+ajaxDate+", "+ajaxYear +"<br>";
                           if (dayEvents.length>0) {
                              thisevent+='<table border =1 style ="width:50%" style="border-collapse: collapse">';
                              thisevent+="<tr><th>Event</th><th>Time</th><th>Category</th><th>Delete</th></tr>";
                              for(e =0;e<dayEvents.length;e++){
                                 thisevent+="<tr><td>"+dayEvents[e].title+ "</td>";
                                 thisevent+= "<td>"+dayEvents[e].time +"</td>";
                                 if (dayEvents[e].category==null) {
                                    thisevent+="<td></td>";
                                 }
                                 else{
                                    thisevent+= "<td>"+dayEvents[e].category+"</td>";
                                 }
                                 thisevent+= '<td><input type="button" value ="Delete" onclick = "deleteEvent(\' '+dayEvents[e].id+'\')"/></td>';
                                 thisevent+="</tr>";
                              }
                              thisevent+="</table>";
                           }
                           else{
                              thisevent+="No events.";
                           }
                           document.getElementById("eventsSpace").innerHTML = thisevent;
                           //document.getElementById("eventsBox").style.display='block';
                           $('#eventsSpace').dialog("open");
                        }
                     }
                  });

            }.bind(thisdate);
           //end of display event 
            
            
            day.appendChild(getEventsButton);
            week.appendChild(day);
         }
         document.getElementById("grid").appendChild(week);
      }
   };
   document.addEventListener("DOMContentLoaded", updateCalendar, false);
   function deleteEvent(event_id){
      $.ajax({
         type:"POST",
         url: "deleteEvent.php",
         data:{
            eventid: event_id
         }, dataType: 'json',
         success: function(data){
            if (data.success == 'false'){
               if(data.error == 'csrf validation failed') {
                  window.alert(data.error);
                  return;
               }
               window.alert(data.error);
               return;
            }
            else{
               updateCalendar();
            }
         }
      });
   }
</script>
<script>
   $(function(){
      $("#dialog").dialog({
         autoOpen:false,
         buttons: {
            Ok: function(){
               request = $.ajax({
                  type:"POST",
                  url: "addEvents.php",
                  data:{
                     title: $("#eventtitle").val(),
                     time: $("#eventtime").val(),
                     date: $("#eventdate").val(),
                     category: $("#categorymenu").val()
                  }, dataType: 'json',
                  success: function(data){
                     console.log(data);
                     console.log(data.success);
                     if (data.success == 'false'){
                        if(data.error == 'csrf validation failed') {
                           window.alert(data.error);
                           return;
                        }
                        window.alert(data.error);
                        return;
                     }                     
                  }
               });
               $(this).dialog("close");
            },
            Cancel:function(){
               $(this).dialog("close");
            }
         }
      });
      $("#createEvent").click(function () {
         $("#dialog").dialog("open");
      });
      $('#eventsSpace').dialog({ autoOpen: false }); // Initialize dialog plugin
      
   });
</script>
</head>
<body>
   <h1 id= "monthtitle"></h1>
   <div id= "buttons">
   <button class ="inner" id='previousMonth' style="float: left">Previous Month</button>
   <button class = "inner" id ='createEvent' onclick= "createEvent"> Create a new Event</button>
   <button class = "inner"id='nextMonth' style="float: right">Next Month</button>
   </div>
   <table border =1 style ='text-align: center' width = '100%' id = 'grid' >
   </table>
   <script type="text/javascript">
      document.getElementById("nextMonth").addEventListener("click", function(event){
         currentMonth = currentMonth.nextMonth(); // Previous month would be currentMonth.prevMonth()
         updateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
         //alert("The new month is "+currentMonth.month+" "+currentMonth.year);
      }, false);
      document.getElementById("previousMonth").addEventListener("click", function(event){
         currentMonth = currentMonth.prevMonth(); // Previous month would be currentMonth.prevMonth()
         updateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
         //alert("The new month is "+currentMonth.month+" "+currentMonth.year);
      }, false);
   </script>
   <div id= "dialog" title= "Add an Event">
      <p>Enter an Event<br>
         <input type= "text" id= "eventtitle">
      </p>
      <p>Enter the time<br>
          <input type="time" id="eventtime">
      </p>
      <p>Enter a date <br>
          <input type="date" id="eventdate">
      </p>
      <p>Select a Category<br>
         <select name="category" id= "categorymenu">
            <option value="1">Home</option>
            <option value="2">School</option>
            <option value="3">Work</option>
            <option value="4">Relaxation</option>
            <option value="5">Excercise</option>
         </select>
      </p>
   </div>
   
   <div id="eventsBox" title ="eventsBox" style="position: relative">
      <div id="eventsSpace" title="Your Events">
      </div>
   </div>
   <form action ="logout.php" method ="GET" id="logoutbutton"><input type = "submit" value = "Log out"></form>
</body>
</html>