<?php
   ini_set("session.cookie_httponly", 1);
   session_start();
   require 'database.php';
   $token = $_SESSION['token'];
   if ($token != $_SESSION['token']){
      echo json_encode(array('success' => 'false', 'error' => 'csrf validation failed'));
      return;
   }
   $user_id = $_SESSION['user_id'];
   $event_id = htmlentities($_POST['eventid']);
   if(isset($_POST['eventid'])){
      $stmt = $mysqli->prepare("delete from events where event_id=?");
      if(!$stmt){
         printf("Query Prep Failed: %s\n", $mysqli->error);
         exit;
      }
      $stmt->bind_param('i',$event_id);
      $stmt->execute();
      $stmt->close();
      echo json_encode(array('success' => 'true'));
    }
    else{
      echo json_encode(array('success' => 'false', 'error' => 'id empty'));
     // return; 
    } 
?>