<?php
   ini_set("session.cookie_httponly", 1);
   session_start();
   require 'database.php';
   $token = $_SESSION['token'];
   if ($token != $_SESSION['token']){
      echo json_encode(array('success' => 'false', 'error' => 'csrf validation failed'));
      return;
   }
   //echo "HI HOW ARE YOU";
   $user_id = $_SESSION['user_id'];
   $day = htmlentities($_GET['day']);
   $month = htmlentities($_GET['month']);
   $year = htmlentities($_GET['year']);
   if(isset($_GET['day'])){
      if(isset($_GET['month'])){
         if(isset($_GET['day'])){
            if($day<10){
               $day="0".$day;
            }
            $requestDate = $year."".$month."".$day;
            $stmt = $mysqli->prepare("select event_id, title, time,category FROM events
                                  WHERE user_id=? and date =?
                                  ORDER by Time ASC");
            if(!$stmt){
               printf("Query Prep Failed: %s\n", $mysqli->error);
               exit;
            }
            $stmt->bind_param('is',$user_id,$requestDate);
            $stmt->execute();
            $stmt->bind_result($event_id, $event_title, $event_time,$event_category);
            $events = array();
            $event = array();
            while($stmt->fetch()){
               $event['title'] = $event_title;
               $event['id'] = $event_id;
               $event['time'] = $event_time;
               //echo $event_category;
               $event['category'] = $event_category;
               $events[] = $event;
            }
            
            echo json_encode(array('success' => 'true','events' => $events));
         }
         else{
            echo json_encode(array('success' => 'false', 'error' => 'year empty'));
            //  return; 
         }
      }
      else{
         echo json_encode(array('success' => 'false', 'error' => 'month empty'));
        // return; 
      }
   }
   else{
      echo json_encode(array('success' => 'false', 'error' => 'day empty'));
     // return; 
   } 
?>